/*
* ========== Structure: Attribute ==========
* Image Classfication
* Daniel Rosen, Shawn Kim, Samriti Kanwar
* CSCI 4502/5502: Data Mining Homework 
* Spring 2013
* Author: Dan Rosen
*/

package ir.structures;

public class Attribute {
    public String attribute;
    public String value;
    
    public Attribute(String att, String val){
        attribute = att;
        value = val;
    }
    
    public Attribute(){
        attribute = "";
        value = "";
    }
    
    public void clear(){
        attribute = "";
        value = "";
    }
    
    @Override
    public String toString(){
        if(attribute.isEmpty()){
            return "";
        }else{
            return attribute+"="+value;
        }
    }
}
