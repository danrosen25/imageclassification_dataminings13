/*
 * ========== Structure: List of Attributes ==========
* Image Classfication
* Daniel Rosen, Shawn Kim, Samriti Kanwar
* CSCI 4502/5502: Data Mining Homework 
* Spring 2013
* Author: Dan Rosen
*/

package ir.structures;
import java.util.StringTokenizer;

public class AttributeList extends java.util.ArrayList<Attribute>{
    
    public AttributeList(){
        super();
    }
    
    public void addList(String attribute, String list){
        StringTokenizer st = new StringTokenizer(list, ";");
        while (st.hasMoreTokens()) {
            this.add(new Attribute(attribute, st.nextToken().trim()));
        }    
    }
    
    public void add(String attribute, String value){
        this.add(new Attribute(attribute,value));
    }
    
    public String printValues(String attribute){
        String values = "";
        for(Attribute attr:this){
            if(attr.attribute.equals(attribute)){
                values+=attr.value+";";
            }
        }
        if(values.length()>0) return values.substring(1,values.length()-1);
        return values;
    }
    
    @Override public String toString(){
        String tmp = "";
        for(Attribute attribute:this)
        {
            tmp = tmp + attribute.toString() + ";";
        }
        if(this.size()>0){
            tmp = tmp + this.get(this.size()-1);
        }
        return tmp;
    }
}
