/*
* ========== Structure: ImageRecord ========== 
* Image Classfication
* Daniel Rosen, Shawn Kim, Samriti Kanwar
* CSCI 4502/5502: Data Mining Homework 
* Spring 2013
* Author: Dan Rosen
*/

package ir.structures;

import java.sql.Date;
import java.text.SimpleDateFormat;

public class ImageRecord{
    public static final int WEB = 1;
    public static final int LOCAL=0;
    
    private int id;
    private int type;
    private String username;
    private String source;
    private String sourceID;
    private String category;
    private String mancategory;
    private String path;
    private Date imageDate;
    private int views;
    private int favorites;
    private AttributeList attributes;
    
    public ImageRecord(String string){
        this();
        String splitted[] = string.split("\\}\\,\\{");
        this.type = Integer.parseInt(splitted[0].substring(1).trim());
        this.username = splitted[1].trim();
        this.source = splitted[2].trim();
        this.sourceID = splitted[3].trim();
        this.category = splitted[4].trim();
        this.path = splitted[5].trim();
        this.views = Integer.parseInt(splitted[6].trim());
        this.favorites = Integer.parseInt(splitted[7].trim());
        this.attributes.addList("tag",splitted[8].trim());
        this.attributes.addList("color",splitted[9].trim());
        this.attributes.addList("pattern",splitted[10].trim());
        this.attributes.addList("shape",splitted[11].trim());
        this.attributes.addList("texture",splitted[12].substring(0,splitted[12].length()-1).trim());
    }
    
    private ImageRecord(){
        this.attributes = new AttributeList();
    }

    public ImageRecord(int id, int type, String username, String source, String sourceID, String category, String mancategory, String path, int views, int favorites,Date imageDate, AttributeList list){
        this(type,username,source,sourceID,category,mancategory, path,views,favorites,imageDate,list);
        this.id = id;
    }
    
    public ImageRecord(int type, String username, String source, String sourceID, String category, String mancategory, String path, int views, int favorites,Date imageDate, AttributeList list){
        this.attributes = list;
        this.type = type;
        this.username = username;
        this.source = source;
        this.sourceID = sourceID;
        this.category = category;
        this.mancategory = mancategory;
        this.path = path;
        this.views = views;
        this.favorites = favorites;
        this.imageDate = imageDate;
    }
    
    public void setCategory(String category){
        this.category = category;
    }
    
    public String getCategory(){
        return category;
    }
    
    public void setManCategory(String category){
        this.mancategory = category;
    }
    
    public String getManCategory(){
        return mancategory;
    }
    
    public void setID(int id){
        this.id = id;
    }
    
    public int getID(){
        return id;
    }
    
    public void setUsername(String uname){
        this.username = uname;
    }
    public String getUsername(){
        return username;
    }
    
    public void setType(int type){
        this.type = type;
    }
    
    public int getType(){
        return type;
    }
    
    public void setSource(String source){
        this.source = source;
    }
    
    public String getSource(){
        return source;
    }
    
    public void setSourceID(String sourceID){
        this.sourceID = sourceID;
    }
    
    public String getSourceID(){
        return sourceID;
    }
    
    public void setPath(String path){
        this.path = path;
    }
    
    public String getPath(){
        return path;
    }
    
    public void setAttributes(AttributeList list){
        this.attributes = list;
    }
    
    public void mergeAttributes(AttributeList list){
        this.attributes.addAll(list);
    }
    
    public void clearAttributes(){
        this.attributes.clear();
    }
    
    public AttributeList getAttributes(){
        return attributes;
    }
    
    public void addAttributeValue(String attribute, String value){
        this.attributes.add(attribute,value);
    }
    
    public int getFavorites(){
        return favorites;
    }
    
    public void setFavorites(int likes){
        this.favorites = likes;
    }
    
    public int getViews(){
        return views;
    }
    
    public void setViews(int popularity){
        this.views = popularity;
    }
    
    public String getTextures(){
        return attributes.printValues("texture");
    }
    
    public void setDate(Date date){
        this.imageDate = date;
    }
    
    public Date getDate(){
        return imageDate;
    }
    
    public String toString(){
        return "{"+id+"},{"+imageDate+"},{"+type+"},{"+username+"},{"+source+"},{"+sourceID+"},{"+category+"},{"+mancategory+"},{"+path+"},{"+views+"},{"+favorites+"},{"+attributes+"}";
    }
    
    /*public static String format(){
        return "{type},{usernam},{source},{sourceID},{category},{path},{popularity},{likes},{tag1;tag2},{color1;color2},{pattern1;pattern2},{shapes1;shapes2},{textures1;textures2}";
    }*/
}
