/*
* ========== Structure for Category ==========
* Image Classfication
* Daniel Rosen, Shawn Kim, Samriti Kanwar
* CSCI 4502/5502: Data Mining Homework 
* Spring 2013
* Author: Dan Rosen
*/

package ir.structures;

public class Category {
    private String code ="";
    private String description = "";
    
    public Category(String att, String val){
        setCode(att);
        setDesc(val);
    }
    
    public Category(){
    }
    
    public void clear(){
        code = "";
        description = "";
    }
    
    public String getCode(){
        return code;
    }
    
    public void setCode(String code){
        this.code = code;
    }
    
    public String getDesc(){
        return description;
    }
    
    public void setDesc(String desc){
        this.description = desc;
    }
    
    @Override
    public String toString(){
        if(code.isEmpty()){
            return "";
        }else{
            return code+": "+description;
        }
    }
}
