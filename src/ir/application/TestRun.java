/*
/* ======== RUN GUI APPLICATION =================
* Image Classfication
* Daniel Rosen, Shawn Kim, Samriti Kanwar
* CSCI 4502/5502: Data Mining Homework 
* Spring 2013
* Author: Dan Rosen
*/

package ir.application;

import ir.flickrinterface.FlickrInterface;
import ir.gui.*;
import ir.dbinterface.*;

public class TestRun {

    public static void main(String[] args) {

        MainGUI display = new MainGUI();  //Displays GUI to view photos
        
    }
}
