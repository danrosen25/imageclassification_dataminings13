/*
 * ========== Interface with the Flickr REST API ==========
* Image Classfication
* Daniel Rosen, Shawn Kim, Samriti Kanwar
* CSCI 4502/5502: Data Mining Homework 
* Spring 2013
* Author: Dan Rosen
*/

package ir.flickrinterface;

import com.flickr4java.flickr.Flickr;
import com.flickr4java.flickr.FlickrException;
import com.flickr4java.flickr.REST;
import com.flickr4java.flickr.interestingness.InterestingnessInterface;
import com.flickr4java.flickr.people.User;
import com.flickr4java.flickr.photos.Photo;
import com.flickr4java.flickr.photos.PhotoList;
import com.flickr4java.flickr.photos.PhotosInterface;
import com.flickr4java.flickr.photos.SearchParameters;
import com.flickr4java.flickr.stats.Stats;
import com.flickr4java.flickr.stats.StatsInterface;
import com.flickr4java.flickr.tags.HotlistTag;
import com.flickr4java.flickr.tags.Tag;
import com.flickr4java.flickr.tags.TagsInterface;
import org.apache.log4j.Logger;
import org.scribe.model.OAuthRequest;

import ir.structures.AttributeList;
import ir.structures.ImageRecord;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.color.CMMException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class FlickrInterface {

    final static String apiKey = "8bed4c7e0e2c182717842a2c48e95fbc";
    final static String sharedSecret = "3278fba2cc8b9081";
    static Flickr flickr;
    static InterestingnessInterface interestInterface;
    static PhotosInterface photosInterface;
    static TagsInterface tagsInterface;
    static StatsInterface statsInterface;

    public FlickrInterface() {
        if (flickr == null) {
            flickr = new Flickr(apiKey, sharedSecret,new REST());
        }
        if (tagsInterface ==null){
            tagsInterface = new TagsInterface(apiKey,sharedSecret,new REST());
        }
        if (interestInterface == null){
            interestInterface = new InterestingnessInterface(apiKey,sharedSecret, new REST());
        }
        if (photosInterface == null){
            photosInterface = new PhotosInterface(apiKey,sharedSecret,new REST());
        }
        if (statsInterface ==null){
            statsInterface = new StatsInterface(apiKey,sharedSecret,new REST());
        }
    }

    public static String extractPhotoID(String url) {
        String photoId = null;

        Matcher m1 = Pattern.compile("photos/[^/]+/(?<img>[0-9]+)").matcher(url);
        if (m1.find()) {
            photoId = m1.group(1);
        } else {
            Matcher m2 = Pattern.compile("flickr.com/[^/]+/(?<img>[0-9]+)_.").matcher(url);
            if (m2.find()) {
                photoId = m2.group(1);
            }
        }
        return photoId;
    }

    public Photo getPhoto(String photoId) {
        try {
            return photosInterface.getInfo(photoId, apiKey);
        } catch (FlickrException ex) {
            //Logger.getLogger(FlickrInterface.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public int getFavoritesOld(String photoId){
        PhotosInterface pi = flickr.getPhotosInterface();
        Collection p = null;
        try {
                p = pi.getFavorites(photoId, 1000, 1);
        } catch (FlickrException ex) {
            //Logger.getLogger(FlickrInterface.class.getName()).log(Level.SEVERE, null, ex);
        }

        return p.size(); 
}
    /*
     * Examples of how to use extractPhotoID() and getPhoto()
     */
    public final static void main(String argv[]) {
        
        FlickrInterface f = new FlickrInterface();
        String photoId1 = f.extractPhotoID("http://www.flickr.com/photos/beccag/68389502/");
        ImageRecord ir = convertToIR(photoId1);
        System.out.println(ir.toString());
        System.out.println(photoId1);
       
        int c1;
        c1 = f.getFavoritesOld(photoId1);
        System.out.println(c1);
        Photo p1 = f.getPhoto(photoId1);
        
        
        for (Object obj : p1.getTags()) {
            Tag t = (Tag) obj;
            System.out.println(t.getValue());
        }
        
        String photoId2 = f.extractPhotoID("http://farm9.staticflickr.com/8261/8634523610_b4e57c6552.jpg");
        System.out.println(photoId2);
        Photo p2 = f.getPhoto(photoId2);
        for (Object obj : p2.getTags()) {
            Tag t = (Tag) obj;
            System.out.println(t.getValue());
        }
    }

    public void testHotList() throws Exception {
        Collection results = tagsInterface.getHotList("week", 200);
        JFrame display = new JFrame();
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        display.setSize(screen.width, screen.height);
        display.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        display.setLayout(new FlowLayout(FlowLayout.LEFT));
        for (Object o : results) {
            HotlistTag tag = (HotlistTag) o;
            JLabel tagL = new JLabel(tag.getValue());
            tagL.setBorder(BorderFactory.createLineBorder(Color.BLACK));
            display.getContentPane().add(tagL);
        }
        display.setVisible(true);
    }
    
    public static java.sql.Date convertDate(java.util.Date date){
        if(date == null){return new java.sql.Date(0);}
        return new java.sql.Date(date.getTime());
    }
    
    public static AttributeList getTags(Photo pinfo){
        Collection tagList = pinfo.getTags();            
        AttributeList taglist = new AttributeList();
        for (Object tag : tagList) {
            taglist.add("tag", ((Tag)tag).getValue());
        }
        return taglist;
    }
    
    public static int getFavorites(String photoID){
        try{
            Stats st = statsInterface.getPhotoStats(photoID, new Date(0));
            if(st == null){return 0;}
            return st.getFavorites();
        }catch(FlickrException e){
            return 0;
        }
    }

    public static int getViews(String photoID){
        try{
            return -1; // No longer works
        }catch(Exception e){
            return 0;
        }
    }
    public static String getUsername(Photo pinfo){
        User u = pinfo.getOwner();
        if(u==null)return "";
        return u.getUsername();
    }
    
    public static void updateFlickrInfo(ImageRecord imageRecord) throws FlickrException{
        try{
            String sourceID = extractPhotoID(imageRecord.getPath());
            if(sourceID == null){
                imageRecord.setSource("NotFlickr");
            }else{
                imageRecord.setSource("Flickr");
                imageRecord.setSourceID(sourceID);
                Photo photo = photosInterface.getPhoto(sourceID);        
                imageRecord.setUsername(getUsername(photo));
                imageRecord.setPath(photo.getSmallUrl());
                imageRecord.setFavorites(getFavorites(sourceID));
                imageRecord.setViews(getViews(sourceID));
                imageRecord.setDate(convertDate(photo.getDatePosted()));
                imageRecord.mergeAttributes(getTags(photo));
            }
        }catch(FlickrException e){
            System.err.println(e);
            throw e;
        }
        
    }
    
    public static ImageRecord convertToIR(String photoID){
        try{
            Photo photo = photosInterface.getPhoto(photoID);
            return new ImageRecord(
                    ImageRecord.WEB,
                    getUsername(photo),
                    "Flickr",
                    photo.getId(),
                    "",
                    "",
                    photo.getSmallUrl(),
                    getViews(photoID),
                    getFavorites(photoID),
                    convertDate(photo.getDatePosted()),
                    getTags(photo));
        }catch(FlickrException e){
            System.err.println(e);
        }
        return null;
    }
    
    public ArrayList<ImageRecord> getInteresting(String inputDate){
        Calendar cal = GregorianCalendar.getInstance();
        try{
            cal.setTime(new SimpleDateFormat("MM/DD/YYYY").parse(inputDate));
            return getInteresting(cal);
        }catch(ParseException e){
            return new ArrayList<>();
        }
    }
    
    public ArrayList<ImageRecord> getInteresting(Calendar cal){
        ArrayList<ImageRecord> images = new ArrayList<>();
        try {
            for (Object o : interestInterface.getList(cal.getTime(), null, 10, 0)) {
                ImageRecord ir = convertToIR(((Photo) o).getId());
                if(ir!=null){
                    images.add(ir);
                }
            }
        }catch( FlickrException e){
            System.err.println(e);
        }
        return images;
    }
    
    public ArrayList<ImageRecord> getPhotosByDay(String inputDate,int number) {

        ArrayList<ImageRecord> images = new ArrayList<>();
        SearchParameters sp = new SearchParameters();
        Calendar cal = GregorianCalendar.getInstance();
        try {
            cal.setTime(new SimpleDateFormat("MM/DD/YYYY").parse(inputDate));
        } catch (ParseException e) {
            System.err.println(e);
        }
        sp.setMinUploadDate(cal.getTime());
        cal.add(Calendar.DATE, 1);
        sp.setMaxUploadDate(cal.getTime());
        
        PhotosInterface pi = flickr.getPhotosInterface();
        try {
            for (Object o : pi.search(sp, 100, 1)) {
                ImageRecord ir = convertToIR(((Photo) o).getId());
                if(ir!=null){
                    images.add(ir);
                }
            }
        } catch (FlickrException e) {
            System.err.println(e);
        }
        return images;
    }

    public void testRecentList() throws Exception {
        PhotoList results = photosInterface.getRecent(null, 100, 1);
        JFrame display = new JFrame();
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        display.setSize(screen.width, screen.height);
        display.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        display.setLayout(new FlowLayout(FlowLayout.LEFT));
        for (Object o : results) {
            Photo photo = (Photo) o;
            String pid = photo.getId();
            Photo pinfo = tagsInterface.getListPhoto(pid);
            Collection tagList = pinfo.getTags();
            String tagString = "";
            for (Object tagO : tagList) {
                Tag tag = (Tag) tagO;
                tagString += tag.getValue() + "; ";
            }
            try {
                Image i = ImageIO.read(new URL(photo.getSmallUrl()));
                JLabel image = new JLabel(new ImageIcon(i));
                JTextArea tags = new JTextArea(tagString, 6, 6);
                tags.setEditable(false);
                tags.setLineWrap(true);
                tags.setWrapStyleWord(true);
                JPanel imageP = new JPanel();
                imageP.setLayout(new BoxLayout(imageP, BoxLayout.Y_AXIS));
                imageP.add(image);
                imageP.add(tags);

                display.getContentPane().add(imageP);
            } catch (CMMException e) {
                System.err.println(e);
            }
            //System.out.println(p.getTitle());     
        }
        display.setVisible(true);
    }
}
