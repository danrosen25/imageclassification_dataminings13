/*
 * ========== Add Flickr Interesting Photos to Photoset ==========
* Image Classfication
* Daniel Rosen, Shawn Kim, Samriti Kanwar
* CSCI 4502/5502: Data Mining Homework 
* Spring 2013
* Author: Dan Rosen
*/

package ir.flickrinterface;

import ir.dbinterface.PhotoTableInterface;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class AddFlickrPhotos {
    public final static void main(String argv[]){
        Calendar cal = GregorianCalendar.getInstance();
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy");
            cal.setTime(sdf.parse("02/02/2012"));
            FlickrInterface f = new FlickrInterface();
            PhotoTableInterface p = new PhotoTableInterface("photoSet","photoSetAttr");
            for(int i=0;i<900;i++){
                p.insertImages(f.getInteresting(cal));
                System.out.println("Done: "+cal.getTime().toString());
                cal.add(Calendar.DATE, 1);
            }

        }catch(ParseException e){
            System.err.println(e);
        }
    }
}
