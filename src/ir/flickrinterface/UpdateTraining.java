/*
 * ========== Get Flickr Tags for ImageNet Photos ==========
* Image Classfication
* Daniel Rosen, Shawn Kim, Samriti Kanwar
* CSCI 4502/5502: Data Mining Homework 
* Spring 2013
* Author: Dan Rosen
*/

package ir.flickrinterface;

import com.flickr4java.flickr.FlickrException;
import ir.dbinterface.PhotoTableInterface;
import ir.structures.ImageRecord;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UpdateTraining {
    public final static void main(String argv[]){
        FlickrInterface f = new FlickrInterface();
        PhotoTableInterface p = new PhotoTableInterface("trainingSet","trainingSetAttr");
        ImageRecord ir;
        for(int i=9000;i<9602;i++){
            if((ir = p.getImageByID(i))!=null){
                try {
                    f.updateFlickrInfo(ir);
                    p.updateImageRefreshAttr(ir);
                } catch (FlickrException ex) {
                    System.err.println(ex);
                }
            }
        }
    }
}
