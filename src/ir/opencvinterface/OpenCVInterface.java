/*
* ========== Use OpenCV to detect features (Not Working) ==========
* Image Classfication
* Daniel Rosen, Shawn Kim, Samriti Kanwar
* CSCI 4502/5502: Data Mining Homework 
* Spring 2013
* Author: Dan Rosen
*/

package ir.opencvinterface;

import com.googlecode.javacv.Blobs;
import static com.googlecode.javacv.cpp.opencv_core.*;
import static com.googlecode.javacv.cpp.opencv_highgui.cvLoadImage;
import static com.googlecode.javacv.cpp.opencv_objdetect.*;
import static com.googlecode.javacv.cpp.opencv_imgproc.*;

import java.io.IOException;
import java.net.URL;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class OpenCVInterface
{
    private static CascadeClassifier cascadeClassifier;
    private static CvMemStorage cvMemStor;
    private static CvHaarClassifierCascade face;
    private static String fileseparator;
    
    public OpenCVInterface(){
        fileseparator = System.getProperty("file.seperator");
        face = new CvHaarClassifierCascade(cvLoad("classifiers"+fileseparator+"haarcascade_fullbody.xml"));
        cvMemStor = cvCreateMemStorage(0);
    }
    
    public static void main(String[] args)
    {
        fileseparator = System.getProperty("file.separator");
        face = new CvHaarClassifierCascade(cvLoad("classifiers"+fileseparator+"haarcascade_frontalface_alt.xml"));
        cvMemStor = cvCreateMemStorage(0);
        
        System.out.println("STARTING...\n");
        String imageURL = "https://sphotos-b.xx.fbcdn.net/hphotos-frc1/901756_10102502617281789_720060682_o.jpg";
        String imagelocal = "images/face.jpg";
        try{
            IplImage image = URL2Ipl(imageURL);
            //IplImage image = Path2Ipl(imagelocal);
            //object_detect(image,face);
            boundURLPic(image);
        }catch(IOException e){
            System.err.println(e);
        }
        System.out.println("ALL DONE");
    }

    public static IplImage Path2Ipl(String url) throws IOException{
        return cvLoadImage(url);
    }
    
    public static IplImage URL2Ipl(String url) throws IOException{
        return IplImage.createFrom(ImageIO.read(new URL(url)));
    }
    
    public static void displayIplImage(IplImage image){
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(new JLabel(new ImageIcon(image.getBufferedImage())));
        frame.pack();
        frame.setVisible(true);
    }
    
    public static void boundURLPic(IplImage RawImage)
    {
        int MinArea = 500;
        int ErodeCount =0;
        int DilateCount = 1; 

        IplImage GrayImage = cvCreateImage(cvGetSize(RawImage), IPL_DEPTH_8U, 1);     
        cvCvtColor(RawImage, GrayImage, CV_BGR2GRAY);
        //ShowImage(GrayImage, "GrayImage", 512);

        IplImage BWImage = cvCreateImage(cvGetSize(GrayImage), IPL_DEPTH_8U, 1); 
        cvThreshold(GrayImage, BWImage, 127, 255, CV_THRESH_BINARY);
        //ShowImage(BWImage, "BWImage");

        IplImage WorkingImage = cvCreateImage(cvGetSize(BWImage), IPL_DEPTH_8U, 1);     
        cvErode(BWImage, WorkingImage, null, ErodeCount);    
        cvDilate(WorkingImage, WorkingImage, null, DilateCount);
        //ShowImage(WorkingImage, "WorkingImage", 512);

        //cvSaveImage("Images/Working.jpg", WorkingImage);
        //PrintGrayImage(WorkingImage, "WorkingImage");
        //BinaryHistogram(WorkingImage);

        Blobs Regions = new Blobs();
        Regions.BlobAnalysis(
                WorkingImage,               // image
                -1, -1,                     // ROI start col, row
                -1, -1,                     // ROI cols, rows
                0,                          // border (0 = black; 1 = white)
                MinArea);                   // minarea
        Regions.PrintRegionData();

        for(int i = 1; i <= Blobs.MaxLabel; i++)
        {
            double [] Region = Blobs.RegionData[i];
            int Parent = (int) Region[Blobs.BLOBPARENT];
            int Color = (int) Region[Blobs.BLOBCOLOR];
            int MinX = (int) Region[Blobs.BLOBMINX];
            int MaxX = (int) Region[Blobs.BLOBMAXX];
            int MinY = (int) Region[Blobs.BLOBMINY];
            int MaxY = (int) Region[Blobs.BLOBMAXY];
            Highlight(RawImage,  MinX, MinY, MaxX, MaxY, 1);
        }

        displayIplImage(RawImage);

        cvReleaseImage(GrayImage); GrayImage = null;
        cvReleaseImage(BWImage); BWImage = null;
        cvReleaseImage(WorkingImage); WorkingImage = null;
  
        cvReleaseImage(RawImage); RawImage = null;

    }
    
    public static void object_detect(IplImage original, CvHaarClassifierCascade classifier){
        
        
        cvClearMemStorage(cvMemStor);
        CvPoint pt1 = new CvPoint();
        CvPoint pt2 = new CvPoint();
        int i;
        cvClearMemStorage(cvMemStor);
        if(classifier != null){
            CvSeq object = cvHaarDetectObjects(original,classifier,cvMemStor,1.2, 2,0);
            for(i = 0;i<object.total();i++){
                CvRect r = new CvRect(cvGetSeqElem(object,i));
                pt1.x((r.x()));
                pt2.x((r.x()+r.width()));
                pt1.y((r.y()));
                pt2.y((r.y()+r.height()));
                cvRectangle(original,pt1,pt2,CV_RGB(255,0,0),3,IPL_DEPTH_8U,3);
            }
        }else{
            System.out.println("No classifier\n");
        }
        displayIplImage(original);
    }
    

    
    public static void Highlight(IplImage image, int [] inVec)
    {
        Highlight(image, inVec[0], inVec[1], inVec[2], inVec[3], 1);
    }
    public static void Highlight(IplImage image, int [] inVec, int Thick)
    {
        Highlight(image, inVec[0], inVec[1], inVec[2], inVec[3], Thick);
    }
    public static void Highlight(IplImage image, int xMin, int yMin, int xMax, int yMax)
    {
        Highlight(image, xMin, yMin, xMax, yMax, 1);
    }
    public static void Highlight(IplImage image, int xMin, int yMin, int xMax, int yMax, int Thick)
    {
        CvPoint pt1 = cvPoint(xMin,yMin);
        CvPoint pt2 = cvPoint(xMax,yMax);
        CvScalar color = cvScalar(255,0,0,0);       // blue [green] [red]
        cvRectangle(image, pt1, pt2, color, Thick, 4, 0);
    }
    
    public static void PrintGrayImage(IplImage image, String caption)
    {
        int size = 512; // impractical to print anything larger
        CvMat mat = image.asCvMat();
        int cols = mat.cols(); if(cols < 1) cols = 1;
        int rows = mat.rows(); if(rows < 1) rows = 1;
        double aspect = 1.0 * cols / rows;
        if(rows > size) { rows = size; cols = (int) ( rows * aspect ); }
        if(cols > size) cols = size;
        rows = (int) ( cols / aspect );
        PrintGrayImage(image, caption, 0, cols, 0, rows);
    }
    public static void PrintGrayImage(IplImage image, String caption, int MinX, int MaxX, int MinY, int MaxY)
    {
        int size = 512; // impractical to print anything larger
        CvMat mat = image.asCvMat();
        int cols = mat.cols(); if(cols < 1) cols = 1;
        int rows = mat.rows(); if(rows < 1) rows = 1;
        
        if(MinX < 0) MinX = 0; if(MinX > cols) MinX = cols; 
        if(MaxX < 0) MaxX = 0; if(MaxX > cols) MaxX = cols; 
        if(MinY < 0) MinY = 0; if(MinY > rows) MinY = rows; 
        if(MaxY < 0) MaxY = 0; if(MaxY > rows) MaxY = rows; 
        
        System.out.println("\n" + caption);
        System.out.print("   +");
        for(int icol = MinX; icol < MaxX; icol++) System.out.print("-");
        System.out.println("+");
        
        for(int irow = MinY; irow < MaxY; irow++)
        {
            if(irow<10) System.out.print(" ");
            if(irow<100) System.out.print(" ");
            System.out.print(irow);
            System.out.print("|");
            for(int icol = MinX; icol < MaxX; icol++)
            {
                int val = (int) mat.get(irow,icol);
                String C = " ";
                if(val == 0) C = "*";
                System.out.print(C);
            }
            System.out.println("|");
        }
        System.out.print("   +");
        for(int icol = MinX; icol < MaxX; icol++) System.out.print("-");
        System.out.println("+");
    }

    public static void PrintImageProperties(IplImage image)
    {
        CvMat mat = image.asCvMat();
        int cols = mat.cols();
        int rows = mat.rows();
        int depth = mat.depth();
        System.out.println("ImageProperties for " + image + " : cols=" + cols + " rows=" + rows + " depth=" + depth);
    }
    
    public static float BinaryHistogram(IplImage image)
    {
        CvScalar Sum = cvSum(image);
        float WhitePixels = (float) ( Sum.getVal(0) / 255 );
        CvMat mat = image.asCvMat();
        float TotalPixels = mat.cols() * mat.rows();
        //float BlackPixels = TotalPixels - WhitePixels;
        return WhitePixels / TotalPixels;
    }
  
    // Counterclockwise small angle rotation by skewing - Does not stretch border pixels
    public static IplImage SkewGrayImage(IplImage Src, double angle)    // angle is in radians
    {
        //double radians = - Math.PI * angle / 360.0;   // Half because skew is horizontal and vertical
        double sin = - Math.sin(angle);
        double AbsSin = Math.abs(sin);
        
        int nChannels = Src.nChannels();
        if(nChannels != 1) 
        {
            System.out.println("ERROR: SkewGrayImage: Require 1 channel: nChannels=" + nChannels);
            System.exit(1);
        }
        
        CvMat SrcMat = Src.asCvMat();
        int SrcCols = SrcMat.cols();
        int SrcRows = SrcMat.rows();

        double WidthSkew = AbsSin * SrcRows; 
        double HeightSkew = AbsSin * SrcCols;
        
        int DstCols = (int) ( SrcCols + WidthSkew ); 
        int DstRows = (int) ( SrcRows + HeightSkew );
    
        CvMat DstMat = cvCreateMat(DstRows, DstCols, CV_8UC1);  // Type matches IPL_DEPTH_8U
        cvSetZero(DstMat);
        cvNot(DstMat, DstMat);
        
        for(int irow = 0; irow < DstRows; irow++)
        {
            int dcol = (int) ( WidthSkew * irow / SrcRows );
            for(int icol = 0; icol < DstCols; icol++)
            {
                int drow = (int) ( HeightSkew - HeightSkew * icol / SrcCols );
                int jrow = irow - drow;
                int jcol = icol - dcol;
                if(jrow < 0 || jcol < 0 || jrow >= SrcRows || jcol >= SrcCols) DstMat.put(irow, icol, 255);
                else DstMat.put(irow, icol, (int) SrcMat.get(jrow,jcol));
            }
        }
        
        IplImage Dst = cvCreateImage(cvSize(DstCols, DstRows), IPL_DEPTH_8U, 1);
        Dst = DstMat.asIplImage();
        return Dst;
    }
    
    public static IplImage TransposeImage(IplImage SrcImage)
    {
        CvMat mat = SrcImage.asCvMat();
        int cols = mat.cols();
        int rows = mat.rows();
        IplImage DstImage = cvCreateImage(cvSize(rows, cols), IPL_DEPTH_8U, 1);
        cvTranspose(SrcImage, DstImage);
        cvFlip(DstImage,DstImage,1);
        return DstImage;
    }
}

