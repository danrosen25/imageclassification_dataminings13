/*
 * ========== Display of attribute type and value ==========
* Image Classfication
* Daniel Rosen, Shawn Kim, Samriti Kanwar
* CSCI 4502/5502: Data Mining Homework 
* Spring 2013
* Author: Dan Rosen
*/

package ir.gui;

import ir.structures.Attribute;
import javax.swing.JTextField;

public class SingleAttributeBox extends javax.swing.JPanel{
    private Attribute attribute;
    private JTextField attributeTB;
    private JTextField valueTB;
    
    public SingleAttributeBox(){
        super();
        attribute = new Attribute();
        attributeTB = new JTextField(10);
        valueTB = new JTextField(10);
        this.add(attributeTB);
        this.add(valueTB);
    }
    
    public SingleAttributeBox(Attribute attribute){
        super();
        this.attribute = attribute;
        attributeTB = new JTextField(attribute.attribute,10);
        valueTB = new JTextField(attribute.value,10);
        this.add(attributeTB);
        this.add(valueTB);
    }
    
    public void clear(){
        attribute.clear();
        attributeTB.setText("");
        valueTB.setText("");
    }
    
    public Boolean isEmpty(){
        return attribute.attribute.isEmpty();
    }
    
    public void setAttribute(Attribute attribute){
        this.attribute = attribute;
        attributeTB.setText(attribute.attribute);
        attributeTB.setText(attribute.value);
    }
    
    public void setValues(String attribute,String value){
        this.attribute.attribute = attribute;
        this.attribute.value = value;
        attributeTB.setText(attribute);
        valueTB.setText(value);
    }
    
    public Attribute getAttribute(){
        return attribute;
    }
    
    public String toString(){
        return attribute.toString();
    }
}
