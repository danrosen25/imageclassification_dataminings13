/*
* ========== Display dropdown menu for tables ==========
* Image Classfication
* Daniel Rosen, Shawn Kim, Samriti Kanwar
* CSCI 4502/5502: Data Mining Homework 
* Spring 2013
* Author: Dan Rosen
*/

package ir.gui;

import javax.swing.JComboBox;

public class TableSelect extends JComboBox<String>{
    private static String training = "Training";
    private static String test = "Test";
    
    public TableSelect(){
        super();
        this.addItem(training);
        this.addItem(test);
    }
    
    public String getTable(){
        if(this.getSelectedItem().equals(training)) return "trainingSet";
        if(this.getSelectedItem().equals(test)) return "photoSet";
        return "";
    }
    
    public String getAttrTable(){
        if(this.getSelectedItem().equals(training)) return "trainingSetAttr";
        if(this.getSelectedItem().equals(test)) return "photoSetAttr";
        return "";
    }
    
}
