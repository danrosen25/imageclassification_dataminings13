/*
* ========== Drop down menu for categories ==========
* Image Classfication
* Daniel Rosen, Shawn Kim, Samriti Kanwar
* CSCI 4502/5502: Data Mining Homework 
* Spring 2013
* Author: Dan Rosen
*/

package ir.gui;

import ir.structures.Category;
import java.util.ArrayList;

public class CategorySelect extends javax.swing.JComboBox<Category>{
    public static final Category all = new Category("*","all");
    
    public CategorySelect(){
        super();
        this.addItem(all);
    }
    
    public CategorySelect(ArrayList<Category> categories){
        this();
        setCategories(categories);
    }
    
    public final void clearCategories(){
        this.removeAllItems();
        this.addItem(all);
    }
    
    public final void setCategories(ArrayList<Category> categories){
        clearCategories();
        for(Category category:categories){
            this.addItem(category);
        }
    }
    
    public final String getCategory(){
        return ((Category)this.getSelectedItem()).getCode();
    }
}
