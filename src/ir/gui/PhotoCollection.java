/*
* ========== Display set of photos ==========
* Image Classfication
* Daniel Rosen, Shawn Kim, Samriti Kanwar
* CSCI 4502/5502: Data Mining Homework 
* Spring 2013
* Author: Dan Rosen
*/

package ir.gui;

import ir.dbinterface.PhotoTableInterface;
import ir.structures.Category;
import ir.structures.ImageRecord;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JPanel;

public class PhotoCollection extends javax.swing.JDialog implements ActionListener{
    JPanel bottomControls;
    JPanel selectionPanel;
    CategorySelect categorySelect;
    FeatureSelect featureSelect;
    JPanel photoDisplay;
    private static int previous_start = 0;
    private static int current_start = 0;
    private static int last_image = 0;
    private static final int MAX_DISPLAY = 20;
    private static final int DEFAULT_START = 0;
    private static final int DEFAULT_HEIGHT = 200;
    private GridBagConstraints c;
    PhotoTableInterface tableInterface;
    
    public PhotoCollection(Frame frame, PhotoTableInterface tableInterface, String set){
        super(frame,"Photos:"+set);
        this.setLayout(new BorderLayout());
        this.setSize(Toolkit.getDefaultToolkit().getScreenSize());
        this.tableInterface = tableInterface;
        
        selectionPanel = new JPanel();
        selectionPanel.setLayout(new GridBagLayout());
        selectionPanel.setSize(300, 20);
        GridBagConstraints selctionLayout = new GridBagConstraints();
        selctionLayout.anchor = GridBagConstraints.LINE_START;  
        categorySelect = new CategorySelect(tableInterface.getCategories());
        featureSelect = new FeatureSelect(tableInterface.getTags());
        selctionLayout.gridx = 0;
        selctionLayout.gridy = 0;
        selectionPanel.add(categorySelect,selctionLayout);
        selctionLayout.gridx = 1;
        selctionLayout.gridy = 0;
        selectionPanel.add(featureSelect,selctionLayout);
        this.getContentPane().add(selectionPanel,BorderLayout.PAGE_START);
        
        bottomControls = new JPanel();
        bottomControls.setLayout(new FlowLayout());
        JButton firstButton = new JButton("First");
        firstButton.setActionCommand("First");
        firstButton.addActionListener(this);
        bottomControls.add(firstButton);
        JButton prevButton = new JButton("Previous");
        prevButton.setActionCommand("Previous");
        prevButton.addActionListener(this);
        bottomControls.add(prevButton);
        JButton nextButton = new JButton("Next");
        nextButton.setActionCommand("Next");
        nextButton.addActionListener(this);
        bottomControls.add(nextButton);
        JButton closeButton = new JButton("Close");
        closeButton.setActionCommand("Close");
        closeButton.addActionListener(this);
        bottomControls.add(closeButton);
        getContentPane().add(bottomControls,BorderLayout.PAGE_END);
        
        photoDisplay = new JPanel(new GridBagLayout());
        c = new GridBagConstraints();
        c.anchor = GridBagConstraints.LINE_START; 
        c.fill = GridBagConstraints.HORIZONTAL;
        this.getContentPane().add(photoDisplay,BorderLayout.CENTER);
        displayCollection(DEFAULT_START);
        
        this.setVisible(true);
    }
    
    private void clear(){
        photoDisplay.removeAll();
        this.repaint();
    }
    
    private void displayPrevious(){
        displayCollection(previous_start);
    }
    
    private void displayNext(){
        displayCollection(last_image+1);
    }
    
    private void displayCollection(int start){
        previous_start = current_start;
        current_start = start;
        clear();
        c.gridx = 0;
        c.gridy = 0;
        last_image = -1;
        if(categorySelect.getCategory().equals("*")){
            if(featureSelect.getValue().equals("*")){
                for(ImageRecord ir:tableInterface.getPage(start, MAX_DISPLAY)){
                    photoDisplay.add(PhotoDisplay.getPhotoPanel(ir,DEFAULT_HEIGHT),c);
                    c.gridx++;
                    if(c.gridx == 5){
                        c.gridy++;
                        c.gridx=0;
                    }
                    if(ir.getID()>last_image){last_image = ir.getID();}
                }
            }
            else{
                for(ImageRecord ir:tableInterface.getPageTag(start, MAX_DISPLAY,featureSelect.getAttribute(),featureSelect.getValue())){
                    photoDisplay.add(PhotoDisplay.getPhotoPanel(ir,DEFAULT_HEIGHT),c);
                    c.gridx++;
                    if(c.gridx == 5){
                        c.gridy++;
                        c.gridx=0;
                    }
                    if(ir.getID()>last_image){last_image = ir.getID();}
                }
            }
        }else{
            if(featureSelect.getValue().equals("*")){
                for(ImageRecord ir:tableInterface.getPageCategory(start, MAX_DISPLAY,categorySelect.getCategory())){
                    photoDisplay.add(PhotoDisplay.getPhotoPanel(ir,DEFAULT_HEIGHT),c);
                    c.gridx++;
                    if(c.gridx == 5){
                        c.gridy++;
                        c.gridx=0;
                    }
                    if(ir.getID()>last_image){last_image = ir.getID();}
                }
            }else{
                for(ImageRecord ir:tableInterface.getPageCatgTag(start, MAX_DISPLAY,categorySelect.getCategory(),featureSelect.getAttribute(),featureSelect.getValue())){
                    photoDisplay.add(PhotoDisplay.getPhotoPanel(ir,DEFAULT_HEIGHT),c);
                    c.gridx++;
                    if(c.gridx == 5){
                        c.gridy++;
                        c.gridx=0;
                    }
                    if(ir.getID()>last_image){last_image = ir.getID();}
                }
            }
        }
        this.pack();
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()){
            case "First":
                displayCollection(DEFAULT_START);
                break;
            case "Previous":
                displayPrevious();
                break;
            case "Next":
                displayNext();
                break;
            case "Close":
                tableInterface.close();
                this.dispose();
                break;
        }
    }
}
