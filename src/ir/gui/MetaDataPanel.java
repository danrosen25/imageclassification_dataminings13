/*
* ========== Display photo metadata ==========
* Image Classfication
* Daniel Rosen, Shawn Kim, Samriti Kanwar
* CSCI 4502/5502: Data Mining Homework 
* Spring 2013
* Author: Dan Rosen
*/

package ir.gui;

import ir.structures.AttributeList;
import ir.structures.ImageRecord;
import static java.awt.Component.LEFT_ALIGNMENT;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.sql.Date;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class MetaDataPanel extends javax.swing.JPanel{
    private TypeGroup typeGroup = new TypeGroup();
    private LabeledField username = new LabeledField(new JLabel("Username: "), new JTextField(15));
    private LabeledField source = new LabeledField(new JLabel("Source: "), new JTextField(15));
    private LabeledField sourceID = new LabeledField(new JLabel("SourceID: "), new JTextField(15));
    private LabeledField category = new LabeledField(new JLabel("Category: "), new JTextField(15));
    private LabeledField mancategory = new LabeledField(new JLabel("Manual Category: "), new JTextField(15));
    private LabeledField views = new LabeledField(new JLabel("Views: "), new JTextField(15));
    private LabeledField favorites = new LabeledField(new JLabel("Favorites: "), new JTextField(15));
    private LabeledField path = new LabeledField(new JLabel("Path: "), new JTextField(50));
    private LabeledField date = new LabeledField(new JLabel("Date: "),new JTextField(15));

    
    public MetaDataPanel(){
        super(new GridBagLayout());
        this.setSize(300, 500);
        GridBagConstraints c = new GridBagConstraints();
        this.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        this.setAlignmentY(LEFT_ALIGNMENT);
        c.anchor = GridBagConstraints.EAST;
        
        c.weightx = 1.0;
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth =1;
        this.add(typeGroup,c);

        c.weightx = 1.0;
        c.gridx = 0;
        c.gridy = 1;  
        c.gridwidth = 1;
        this.add(username,c);
        
        c.weightx = 1.0;
        c.gridx = 1;
        c.gridy = 1;
        c.gridwidth = 1;
        this.add(category,c);
        
        c.weightx = 0.5;
        c.gridx = 0;
        c.gridy = 2;
        c.gridwidth =1;
        this.add(date,c);
        
        c.weightx = 0.5;
        c.gridx = 1;
        c.gridy = 2;
        c.gridwidth =1;
        this.add(mancategory,c);
        
        c.weightx = 0.5;
        c.gridx = 0;
        c.gridy = 3;
        c.gridwidth =1;
        this.add(source,c);

        c.weightx = 0.5;
        c.gridx = 1;
        c.gridy = 3;
        c.gridwidth =1;
        this.add(sourceID,c);

        c.weightx = 0.5;
        c.gridx = 0;
        c.gridy = 4;
        c.gridwidth =1;
        this.add(views,c);

        c.weightx = 0.5;
        c.gridx = 1;
        c.gridy = 4;
        c.gridwidth =1;
        this.add(favorites,c);

        c.weightx = 1.0;
        c.gridx = 0;
        c.gridy = 5;
        c.gridwidth =2;
        this.add(path,c);
        

    }
    
    public int getType(){
        return typeGroup.getType();
    }
    
    public String getUsername(){
        return username.getText();
    }
    
    
    public String getSource(){
        return source.getText();
    }
    
    public String getSourceID(){
        return sourceID.getText();
    }
    
    public String getCategory(){
        return category.getText();
    }
    
    public String getManCategory(){
        return mancategory.getText();
    }
    
    public String getPath(){
        return path.getText();
    }
    
    public int getViews(){
        return views.getInt();
    }
    
    public int getFavorites(){
        return favorites.getInt();
    }
    
    public Date getDate(){
        return date.getDate();
    }
    


    public void setRecord(ImageRecord record){
        clearRecord();
        if(record != null){
            typeGroup.setType(record.getType());
            username.setText(record.getUsername());
            source.setText(record.getSource());
            sourceID.setText(record.getSourceID());
            category.setText(record.getCategory());
            mancategory.setText(record.getManCategory());
            path.setText(record.getPath());
            views.setInt(record.getViews());
            favorites.setInt(record.getFavorites());
            date.setDate(record.getDate());
        }
    }

    public void clearRecord(){
        typeGroup.setType(ImageRecord.WEB);
        username.clearText();
        source.clearText();
        sourceID.clearText();
        category.clearText();
        mancategory.clearText();
        path.clearText();
        views.clearText();
        favorites.clearText();
    }
}
