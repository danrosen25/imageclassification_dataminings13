/*
* ========== Display GUI Options ========== 
* Image Classfication
* Daniel Rosen, Shawn Kim, Samriti Kanwar
* CSCI 4502/5502: Data Mining Homework 
* Spring 2013
* Author: Dan Rosen
*/

package ir.gui;
import ir.fileinterface.FileInterface;
import ir.flickrinterface.FlickrInterface;
import ir.dbinterface.PhotoTableInterface;
import ir.structures.ImageRecord;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import javax.swing.*;

public class MainGUI extends JFrame implements ActionListener{
    JPanel topControls;
    JPanel bottomControls;
    TableSelect table;

    FlickrInterface fl;
            
    public MainGUI(){
        super("Options");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        
        topControls = new JPanel();
        JButton collection = new JButton("View");
        collection.setActionCommand("View");
        collection.addActionListener(this);
        topControls.add(collection);
        JButton edit = new JButton("Photo Editor");
        edit.setActionCommand("Edit");
        edit.addActionListener(this);
        topControls.add(edit);
        JButton exportAll = new JButton ("Export All");
        exportAll.setActionCommand("Export");
        exportAll.addActionListener(this);
        topControls.add(exportAll);
        JButton importFile = new JButton("Import File");
        importFile.setActionCommand("ImportFile");
        importFile.addActionListener(this);
        topControls.add(importFile);
        JButton dispFlickr = new JButton("Display Flickr");
        dispFlickr.setActionCommand("DispFlickr");
        dispFlickr.addActionListener(this);
        topControls.add(dispFlickr);
        getContentPane().add(topControls,BorderLayout.PAGE_START);
        
        table = new TableSelect();
        getContentPane().add(table,BorderLayout.CENTER);

        bottomControls = new JPanel();
        JButton close = new JButton("Close");
        close.setActionCommand("Close");
        close.addActionListener(this);
        bottomControls.add(close);
        getContentPane().add(bottomControls,BorderLayout.PAGE_END);
        
        pack();
        setVisible(true);
    }
    
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()){
            case "View":
                PhotoCollection pCollection = new PhotoCollection(this,new PhotoTableInterface(table.getTable(),table.getAttrTable()),table.getTable());
                pack();
                break;
            case "Edit":
                PhotoEditor pEdit = new PhotoEditor(this,new PhotoTableInterface(table.getTable(),table.getAttrTable()),table.getTable());
                break;
            case "ImportFile":
                FileInterface.loadFile(JOptionPane.showInputDialog("File?"));
                JOptionPane.showMessageDialog(this, "Done.");
                break;
            case "Export":
                /*FileInterface.outFile(JOptionPane.showInputDialog("File?"));
                JOptionPane.showMessageDialog(this, "Done.");*/
                break;
            case "DispFlickr": 
                fl = new FlickrInterface();
                String date = JOptionPane.showInputDialog("Date (MM/DD/YYYY)?");
                //photoDisplay.displayCollection(fl.getPhotosByDay(date,10),100);
                pack();
                break;
            case "Close":
                this.dispose();         
                break;
        }
    }
    
}
