/*
* ========== Drop down menu for features ==========
* Image Classfication
* Daniel Rosen, Shawn Kim, Samriti Kanwar
* CSCI 4502/5502: Data Mining Homework 
* Spring 2013
* Author: Dan Rosen
*/

package ir.gui;

import ir.structures.Attribute;
import java.util.ArrayList;

public class FeatureSelect extends javax.swing.JComboBox<Attribute>{
    public static final Attribute all = new Attribute("all","*");
    
    public FeatureSelect(){
        super();
        this.addItem(all);
    }
    
    public FeatureSelect(ArrayList<Attribute> categories){
        this();
        setAttribute(categories);
    }
    
    public final void clear(){
        this.removeAllItems();
        this.addItem(all);
    }
    
    public final void setAttribute(ArrayList<Attribute> categories){
        clear();
        for(Attribute category:categories){
            this.addItem(category);
        }
    }
    
    public final String getAttribute(){
        return ((Attribute)this.getSelectedItem()).attribute;
    }
    public final String getValue(){
        return ((Attribute)this.getSelectedItem()).value;
    }
}
