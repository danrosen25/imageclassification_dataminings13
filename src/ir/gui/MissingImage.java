/*
* ========== Display box for missing image ==========
* Image Classfication
* Daniel Rosen, Shawn Kim, Samriti Kanwar
* CSCI 4502/5502: Data Mining Homework 
* Spring 2013
* Author: Dan Rosen
*/

package ir.gui;

import javax.swing.BorderFactory;

public class MissingImage extends javax.swing.JLabel {
    public MissingImage(){
        super("Missing Image");
        this.setBorder(BorderFactory.createTitledBorder("Missing"));
    }
}
