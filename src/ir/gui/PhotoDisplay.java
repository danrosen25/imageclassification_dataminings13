/*
* ========== Display single photo ==========
* Image Classfication
* Daniel Rosen, Shawn Kim, Samriti Kanwar
* CSCI 4502/5502: Data Mining Homework 
* Spring 2013
* Author: Dan Rosen
*/

package ir.gui;

import ir.structures.ImageRecord;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class PhotoDisplay{
    private static MissingImage missing = new MissingImage();

    private static Image getImage(int type, String path) throws IOException{
        try{
            if(type==ImageRecord.WEB){
                return ImageIO.read(new URL(path));
            }else{
                return ImageIO.read(new File(path));
            }
        }catch(IOException e){
            System.err.println(e+" Path="+path);
            throw e;
        }
    }
    
    private static Image getScaledImage(int type, String path, int height) throws IOException, NullPointerException{
        try{
            return getImage(type,path).getScaledInstance(-1, height, Image.SCALE_DEFAULT);
        }catch(IOException | NullPointerException e){
            throw e;
        }
    }
    
    private static Image getImage(ImageRecord imageRecord, int height) throws IOException, NullPointerException {
        try{
            return getScaledImage(imageRecord.getType(),imageRecord.getPath(),height);
        }catch(IOException | NullPointerException e){
            throw e;
        }
    }
    
    public static JPanel getPhotoPanel(ImageRecord ir, int height){
        JPanel iPanel = new JPanel();
        iPanel.setLayout(new BoxLayout(iPanel,BoxLayout.Y_AXIS));
        try{
            JLabel image = new JLabel(new ImageIcon(getImage(ir,height)));
            iPanel.add(image);
        }catch(IOException | NullPointerException e){
            iPanel.add(missing);
        }
        return iPanel;
    }
    
    public static JPanel getPhotoPanel(String path, int height){
        JPanel iPanel = new JPanel();
        iPanel.setLayout(new BoxLayout(iPanel,BoxLayout.Y_AXIS));
        try{
            JLabel image = new JLabel(new ImageIcon(getScaledImage(ImageRecord.WEB,path,height)));
            iPanel.add(image);
        }catch(IOException | NullPointerException e){
            iPanel.add(missing);
        }
        return iPanel;
    }
    





}
