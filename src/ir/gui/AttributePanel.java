/*
 * ========== Display checkboxes for 25 attributes and current attributes ==========
* Image Classfication
* Daniel Rosen, Shawn Kim, Samriti Kanwar
* CSCI 4502/5502: Data Mining Homework 
* Spring 2013
* Author: Dan Rosen
*/

package ir.gui;

import ir.structures.Attribute;
import ir.structures.AttributeList;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import javax.swing.JCheckBox;

public class AttributePanel extends javax.swing.JPanel{
    private JCheckBox black = new JCheckBox("black");
    private JCheckBox blue = new JCheckBox("blue");
    private JCheckBox brown = new JCheckBox("brown");
    private JCheckBox gray = new JCheckBox("gray");
    private JCheckBox green = new JCheckBox("green");
    private JCheckBox orange = new JCheckBox("orange");
    private JCheckBox pink = new JCheckBox("pink");
    private JCheckBox red = new JCheckBox("red");
    private JCheckBox violet = new JCheckBox("violet");
    private JCheckBox white = new JCheckBox("white");
    private JCheckBox yellow = new JCheckBox("yellow");    
    private JCheckBox spotted = new JCheckBox("spotted");
    private JCheckBox striped = new JCheckBox("striped");
    private JCheckBox longCB = new JCheckBox("long");
    private JCheckBox round = new JCheckBox("round");
    private JCheckBox rectangular = new JCheckBox("rectangular");
    private JCheckBox square = new JCheckBox("square");
    private JCheckBox furry = new JCheckBox("furry");
    private JCheckBox smooth = new JCheckBox("smooth");
    private JCheckBox rough = new JCheckBox("rough");
    private JCheckBox shiny = new JCheckBox("shiny");
    private JCheckBox metallic = new JCheckBox("metallic");
    private JCheckBox vegetation = new JCheckBox("vegetation");
    private JCheckBox wooden = new JCheckBox("wooden");
    private JCheckBox wet = new JCheckBox("wet");
    
    private AttributeListDisplay attributes = new AttributeListDisplay();
    
    public AttributePanel(){
        super(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.LINE_START;
        
        c.gridx = 0;
        c.gridy = 0;
        this.add(black,c);
        c.gridx = 1;
        c.gridy = 0;
        this.add(blue,c);
        c.gridx = 2;
        c.gridy = 0;
        this.add(brown,c);
        c.gridx = 3;
        c.gridy = 0;
        this.add(gray,c);
        c.gridx = 4;
        c.gridy = 0;
        this.add(green,c);
        c.gridx = 0;
        c.gridy = 1;
        this.add(orange,c);
        c.gridx = 1;
        c.gridy = 1;
        this.add(pink,c);
        c.gridx = 2;
        c.gridy = 1;
        this.add(red,c);
        c.gridx = 3;
        c.gridy = 1;
        this.add(violet,c); 
        c.gridx = 4;
        c.gridy = 1;
        this.add(white,c);
        c.gridx = 0;
        c.gridy = 2;
        this.add(yellow,c);
        c.gridx = 1;
        c.gridy = 2;
        this.add(spotted,c);
        c.gridx = 2;
        c.gridy = 2;
        this.add(striped,c);
        c.gridx = 3;
        c.gridy = 2;
        this.add(longCB,c);
        c.gridx = 4;
        c.gridy = 2;
        this.add(round,c);
        c.gridx = 0;
        c.gridy = 3;
        this.add(rectangular,c);
        c.gridx = 1;
        c.gridy = 3;
        this.add(square,c);
        c.gridx = 2;
        c.gridy = 3;
        this.add(furry,c);
        c.gridx = 3;
        c.gridy = 3;
        this.add(smooth,c);
        c.gridx = 4;
        c.gridy = 3;
        this.add(rough,c);
        c.gridx = 0;
        c.gridy = 4;
        this.add(shiny,c);
        c.gridx = 1;
        c.gridy = 4;
        this.add(metallic,c);
        c.gridx = 2;
        c.gridy = 4;
        this.add(vegetation,c);
        c.gridx = 3;
        c.gridy = 4;
        this.add(wooden,c);
        c.gridx = 4;
        c.gridy = 4;
        this.add(wet,c);
        
        c.weightx = 1.0;
        c.gridx = 0;
        c.gridy = 5;
        c.gridwidth =5;
        this.add(attributes,c);
        
    }
    
    public void setAttributes(AttributeList list){
        clearChecked();
        for(Attribute attr:list){
            switch(attr.value){
                case "black": black.setSelected(true); break;
                case "blue": blue.setSelected(true); break;
                case "brown": brown.setSelected(true); break;
                case "gray": gray.setSelected(true); break;
                case "green": green.setSelected(true); break;
                case "orange": orange.setSelected(true); break;
                case "pink": pink.setSelected(true); break;
                case "red": red.setSelected(true); break;
                case "violet": violet.setSelected(true); break;
                case "white": white.setSelected(true); break;
                case "yellow": yellow.setSelected(true); break;
                case "spotted": spotted.setSelected(true); break;
                case "striped": striped.setSelected(true); break;
                case "long": longCB.setSelected(true); break;
                case "round": round.setSelected(true); break;
                case "rectangular": rectangular.setSelected(true); break;
                case "square": square.setSelected(true); break;
                case "furry": furry.setSelected(true); break;
                case "smooth": smooth.setSelected(true); break;
                case "rough": rough.setSelected(true); break;
                case "shiny": shiny.setSelected(true); break;
                case "metallic": metallic.setSelected(true); break;
                case "vegetation": vegetation.setSelected(true); break;
                case "wooden": wooden.setSelected(true); break;
                case "wet": wet.setSelected(true); break;
            }
        }
        attributes.setAttributeList(list);
    }
    
    public AttributeList getAttributess(){
        return attributes.getAttributes();
    }
    
    public AttributeList getChecked(){
        AttributeList list = new AttributeList();
        if(black.isSelected()) list.add("color","black");
        if(blue.isSelected()) list.add("color","blue");
        if(brown.isSelected()) list.add("color","brown");
        if(gray.isSelected()) list.add("color","gray");
        if(green.isSelected()) list.add("color","green");
        if(orange.isSelected()) list.add("color","orange");
        if(pink.isSelected()) list.add("color","pink");
        if(red.isSelected()) list.add("color","red");
        if(violet.isSelected()) list.add("color","violet");
        if(white.isSelected()) list.add("color","white");
        if(yellow.isSelected()) list.add("color","yellow");
        if(spotted.isSelected()) list.add("pattern","spotted");
        if(striped.isSelected()) list.add("pattern","striped");
        if(longCB.isSelected()) list.add("shape","long");
        if(round.isSelected()) list.add("shape","round");
        if(rectangular.isSelected()) list.add("shape","rectangle");
        if(square.isSelected()) list.add("shape","square");
        if(furry.isSelected()) list.add("texture","furry");
        if(smooth.isSelected()) list.add("texture","smooth");
        if(rough.isSelected()) list.add("texture","rough");
        if(shiny.isSelected()) list.add("texture","shiny");
        if(metallic.isSelected()) list.add("texture","metallic");
        if(vegetation.isSelected()) list.add("texture","vegetation");
        if(wooden.isSelected()) list.add("texture","wooden");
        if(wet.isSelected()) list.add("texture","wet");
        return list;
    }
    
    public void clearChecked(){
        black.setSelected(false);
        blue.setSelected(false);
        brown.setSelected(false);
        gray.setSelected(false);
        green.setSelected(false);
        orange.setSelected(false);
        pink.setSelected(false);
        red.setSelected(false);
        violet.setSelected(false);
        white.setSelected(false);
        yellow.setSelected(false);
        spotted.setSelected(false);
        striped.setSelected(false);
        longCB.setSelected(false);
        round.setSelected(false);
        rectangular.setSelected(false);
        square.setSelected(false);
        furry.setSelected(false);
        smooth.setSelected(false);
        rough.setSelected(false);
        shiny.setSelected(false);
        metallic.setSelected(false);
        vegetation.setSelected(false);
        wooden.setSelected(false);
        wet.setSelected(false);
        attributes.clear();
    }
}
