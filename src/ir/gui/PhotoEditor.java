/*
* ========== Display Photo Editor ========== 
* Image Classfication
* Daniel Rosen, Shawn Kim, Samriti Kanwar
* CSCI 4502/5502: Data Mining Homework 
* Spring 2013
* Author: Dan Rosen
*/

package ir.gui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import ir.dbinterface.PhotoTableInterface;
import ir.structures.ImageRecord;

public class PhotoEditor extends javax.swing.JDialog implements ActionListener{
    public static int DEFAULT_HEIGHT = 200;
    
    JPanel topControls;
    JPanel bottomControls;
    JPanel display;

    ImageRecord photoRecord;
    PhotoTableInterface tableInterface;
    
    LabeledField id = new LabeledField(new JLabel("ID:"),new JTextField(10));
    MetaDataPanel metaDataPanel = new MetaDataPanel();
    AttributePanel attributePanel = new AttributePanel();
    JPanel photoDisplay = new JPanel();
    
    public PhotoEditor(Frame frame, PhotoTableInterface tableInterface, String set){
        super(frame,"Photo Editor: "+set);
        this.tableInterface = tableInterface;
        setLayout(new BorderLayout());
        
        topControls = new JPanel();
        topControls.setLayout(new FlowLayout());
        JButton newButton = new JButton("Clear");
        newButton.setActionCommand("Clear");
        newButton.addActionListener(this);
        topControls.add(newButton);
        JButton editButton = new JButton("Clear/Update Features");
        editButton.setActionCommand("UpdateClear");
        editButton.addActionListener(this);
        topControls.add(editButton);
        JButton addButton = new JButton("Add/Update Features");
        addButton.setActionCommand("UpdateAdd");
        addButton.addActionListener(this);
        topControls.add(addButton);
        JButton getButton = new JButton("Get from DB");
        getButton.setActionCommand("Get");
        getButton.addActionListener(this);
        topControls.add(getButton);
        JButton deleteButton = new JButton("Delete");
        deleteButton.setActionCommand("Delete");
        deleteButton.addActionListener(this);
        topControls.add(deleteButton);
        JButton dispButton = new JButton("Display from Web");
        dispButton.setActionCommand("Display");
        dispButton.addActionListener(this);
        topControls.add(dispButton);
        getContentPane().add(topControls,BorderLayout.PAGE_START);
        
        display = new JPanel();
        display.setLayout(new BorderLayout());
        display.add(id,BorderLayout.NORTH);
        display.add(metaDataPanel,BorderLayout.WEST);
        display.add(attributePanel,BorderLayout.EAST);
        photoDisplay.setSize(150,150);
        display.add(photoDisplay,BorderLayout.SOUTH);
        getContentPane().add(display,BorderLayout.CENTER);

        bottomControls = new JPanel();
        bottomControls.setLayout(new FlowLayout());
        JButton importButton = new JButton("Add");
        importButton.setActionCommand("Add");
        importButton.addActionListener(this);
        bottomControls.add(importButton);
        JButton close = new JButton("Close");
        close.setActionCommand("Close");
        close.addActionListener(this);
        bottomControls.add(close);
        getContentPane().add(bottomControls,BorderLayout.PAGE_END);
        
        pack();
        setVisible(true);
    }
    
    private void getPhoto(){
        try{
            photoRecord = tableInterface.getImageByID(Integer.parseInt(id.getText()));
            if(photoRecord != null){
                metaDataPanel.setRecord(photoRecord);
                attributePanel.setAttributes(photoRecord.getAttributes());
                photoDisplay.removeAll();
                photoDisplay.add(PhotoDisplay.getPhotoPanel(photoRecord, DEFAULT_HEIGHT));
                this.pack();
            }else{
                JOptionPane.showMessageDialog(this, "This entry was not found in the database.", "Not in Database",JOptionPane.ERROR_MESSAGE);
            }
        }catch(NumberFormatException e){
            System.err.println(e);
        }
    }
    
    private void displayPhoto(){
        photoDisplay.removeAll();
        photoDisplay.add(PhotoDisplay.getPhotoPanel(metaDataPanel.getPath(), DEFAULT_HEIGHT));
        this.pack();
    }
    
    private void updatePhoto(){
        photoRecord = new ImageRecord(
                Integer.parseInt(id.getText()),
                metaDataPanel.getType(),
                metaDataPanel.getUsername(),
                metaDataPanel.getSource(),
                metaDataPanel.getSourceID(),
                metaDataPanel.getCategory(),
                metaDataPanel.getManCategory(),
                metaDataPanel.getPath(),
                metaDataPanel.getViews(),
                metaDataPanel.getFavorites(),
                metaDataPanel.getDate(),
                attributePanel.getChecked());
        tableInterface.updateImageRefreshAttr(photoRecord);
    }
    
        private void updatePhotoAdd(){
        photoRecord = new ImageRecord(
                Integer.parseInt(id.getText()),
                metaDataPanel.getType(),
                metaDataPanel.getUsername(),
                metaDataPanel.getSource(),
                metaDataPanel.getSourceID(),
                metaDataPanel.getCategory(),
                metaDataPanel.getManCategory(),
                metaDataPanel.getPath(),
                metaDataPanel.getViews(),
                metaDataPanel.getFavorites(),
                metaDataPanel.getDate(),
                attributePanel.getChecked());
        tableInterface.updateImageAddAttr(photoRecord);
    }
    
    private void addPhoto(){
        photoRecord = new ImageRecord(
                metaDataPanel.getType(),
                metaDataPanel.getUsername(),
                metaDataPanel.getSource(),
                metaDataPanel.getSourceID(),
                metaDataPanel.getCategory(),
                metaDataPanel.getManCategory(),
                metaDataPanel.getPath(),
                metaDataPanel.getViews(),
                metaDataPanel.getFavorites(),
                metaDataPanel.getDate(),
                attributePanel.getChecked());
        try{
            id.setText(Integer.toString(tableInterface.insertImage(photoRecord)));
        }catch(NumberFormatException e){
            System.err.println(e);
        }
    }
    
    private void deletePhoto(){
        tableInterface.deletePhoto(Integer.parseInt(id.getText()));
        clearPhoto();
    }
    private void clearPhoto(){
        photoRecord = null;
        id.clearText();
        metaDataPanel.clearRecord();
        attributePanel.clearChecked();
        photoDisplay.removeAll();
        this.pack();
    }
    
    
    @Override
    public void actionPerformed(ActionEvent e) {

        switch(e.getActionCommand()){
            case "Clear":
                clearPhoto();
                break;
            case "UpdateAdd":
                updatePhotoAdd();
                break;
            case "UpdateClear":
                updatePhoto();
                break;
            case "Delete":
                deletePhoto();
                break;
            case "Get":
                getPhoto();
                pack();
                break;
            case "Display":
                displayPhoto();
                pack();
                break;
            case "Add":
                addPhoto();
                break;
            case "Close":
                tableInterface.close();
                this.dispose();
                break;
        }
    }
}
