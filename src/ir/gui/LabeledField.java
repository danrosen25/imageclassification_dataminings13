/*
* ========== Display textbox with label ==========
* Image Classfication
* Daniel Rosen, Shawn Kim, Samriti Kanwar
* CSCI 4502/5502: Data Mining Homework 
* Spring 2013
* Author: Dan Rosen
*/

package ir.gui;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.sql.Date;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class LabeledField extends javax.swing.JPanel{
    JLabel label;
    JTextField text;
    public LabeledField(JLabel label, JTextField text){
        super();
        this.setLayout(new FlowLayout());
        this.label = label;
        this.text = text;
        label.setLabelFor(this.text);
        this.add(label);
        this.add(text);
    }
    
    public String getText(){
        return text.getText();
    }
    
    public int getInt(){
        try{
            return Integer.parseInt(text.getText());
        }catch(NumberFormatException e){
            return 0;
        }
    }
    
    public Date getDate(){
        return Date.valueOf(text.getText());
    }
        
    public void setText(String string){
        text.setText(string);
    }
    
    public void setInt(int number){
        text.setText(Integer.toString(number));
    }
    
    public void setDate(Date date){
        if(date==null){
            text.setText("");
        }else{
            text.setText(date.toString());
        }
    }
    
    public void clearText(){
        text.setText("");
    }
}
