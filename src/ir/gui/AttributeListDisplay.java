/*
* ========== Display all attributes ==========
* Image Classfication
* Daniel Rosen, Shawn Kim, Samriti Kanwar
* CSCI 4502/5502: Data Mining Homework 
* Spring 2013
* Author: Dan Rosen
*/

package ir.gui;

import ir.structures.Attribute;
import ir.structures.AttributeList;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class AttributeListDisplay extends javax.swing.JPanel implements ActionListener{
    static JPanel attributesCheck;
    static JPanel controls;

    
    public AttributeListDisplay(){
        super();
        this.setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
        this.setBorder(BorderFactory.createTitledBorder("Attributes"));
        
        controls = new JPanel();
        attributesCheck = new JPanel();
        attributesCheck.setLayout(new BoxLayout(attributesCheck,BoxLayout.Y_AXIS));
        attributesCheck.setAutoscrolls(true);
        JScrollPane scroll = new JScrollPane(attributesCheck);
        scroll.setPreferredSize(new Dimension(200,100));
        scroll.setBorder(BorderFactory.createEmptyBorder());
        scroll.setAutoscrolls(true);
        
        setControls();

        this.add(scroll);
        this.add(controls);
    }
    
    public AttributeListDisplay(AttributeList list){
        this();
        addAttributeList(list);
    }
    
    private void addNew(){
        addAttribute(new SingleAttributeBox());
    }
    
    private void removeLast(){
        if(attributesCheck.getComponentCount()>0){
            attributesCheck.remove(attributesCheck.getComponentCount()-1);
            attributesCheck.repaint();
        }
    }
    
    private void addAttribute(SingleAttributeBox ab){
        attributesCheck.add(ab);
        attributesCheck.repaint();
    }
    
    public final void addAttributeList(AttributeList list){
        for(Attribute a:list){
            addAttribute(new SingleAttributeBox(a));
        }
    }
    
    public void setAttributeList(AttributeList list){
        clear();
        addAttributeList(list);
    }
    
    public void clear(){
        attributesCheck.removeAll();
        attributesCheck.repaint();
    }
    
    public AttributeList getAttributes(){
        AttributeList returnList = new AttributeList();
        for(int i = 0;i<attributesCheck.getComponentCount();i++){
            returnList.add(((SingleAttributeBox)attributesCheck.getComponent(i)).getAttribute());
        }
        return returnList;
    }
    
    private void setControls(){
        controls.setLayout(new FlowLayout());
        JButton clearButton = new JButton("Clear All");
        clearButton.setActionCommand("Clear");
        clearButton.addActionListener(this);
        controls.add(clearButton);
        JButton addButton = new JButton("Add Line");
        addButton.setActionCommand("Add");
        addButton.addActionListener(this);
        controls.add(addButton);
        JButton removeButton = new JButton("Remove Line");
        removeButton.setActionCommand("Remove");
        removeButton.addActionListener(this);
        controls.add(removeButton);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()){
            case "Clear":
                clear();
                break;
            case "Add":
                addNew();
                break;
            case "Remove":
                removeLast();
                break;
        }
    }
}
