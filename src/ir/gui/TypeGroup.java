/*
* ========== Display Button Group for Type ==========
* Image Classfication
* Daniel Rosen, Shawn Kim, Samriti Kanwar
* CSCI 4502/5502: Data Mining Homework 
* Spring 2013
* Author: Dan Rosen
*/

package ir.gui;

import ir.structures.ImageRecord;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JRadioButton;

public class TypeGroup extends javax.swing.JPanel{
    JLabel typeL = new JLabel("Type");
    ButtonGroup typeGroup = new ButtonGroup();
    JRadioButton local = new JRadioButton("Local");
    JRadioButton web = new JRadioButton("Web");
    
    public TypeGroup(){
        super();
        typeL.setLabelFor(this);
        this.add(typeL);
        this.add(web);
        this.add(local);
        typeGroup.add(web);
        typeGroup.add(local);
        web.setSelected(true);
    }
    
    public int getType(){
        if(web.isSelected()){
            return ImageRecord.WEB;
        }else{
            return ImageRecord.LOCAL;
        }
    }
    
    public void setType(int type){
        switch(type){
            case ImageRecord.WEB:
                web.setSelected(true);
                break;
            case ImageRecord.LOCAL:
                local.setSelected(true);
                break;
        }
    }
}
