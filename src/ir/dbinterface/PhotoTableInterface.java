/*
 * ========== Interface to photo and photo attribute tables ==========
* Image Classfication
* Daniel Rosen, Shawn Kim, Samriti Kanwar
* CSCI 4502/5502: Data Mining Homework 
* Spring 2013
* Author: Dan Rosen
*/

package ir.dbinterface;
import ir.structures.Attribute;
import ir.structures.AttributeList;
import ir.structures.Category;
import ir.structures.ImageRecord;
import java.sql.*;
import java.util.ArrayList;

public class PhotoTableInterface{
    private String tablename = "photoSet";
    private String tableAttributes = "photoSetAttr";
    private static final String tableCatgMap = "categoryDescMap";
    private DataMiningConn db;
    
    public PhotoTableInterface(String tablename, String tableAttributes){
        db = new DataMiningConn();
        setTables(tablename,tableAttributes);
    }
    
    public void setTables(String tablename, String tableAttributes){
        this.tablename = tablename;
        this.tableAttributes = tableAttributes;
    }
    
    public void connect() throws SQLException, InstantiationException, ClassNotFoundException, IllegalAccessException{
        db.connect();
    }
    
    public ArrayList<Category> getCategories(){
        ArrayList<Category> categories = new ArrayList<>();
        try {
            connect();
            PreparedStatement queryCategories = db.prepareStatement("SELECT "+this.tablename+".category,description FROM "+ this.tablename+" LEFT JOIN "+this.tableCatgMap+" ON "+this.tablename+".category="+this.tableCatgMap+".category GROUP BY "+this.tablename+".category");
            ResultSet rs = queryCategories.executeQuery();
            while(rs.next()){
                categories.add(new Category(rs.getString("category"),rs.getString("description")));
            }
            return categories;
        } catch (SQLException | InstantiationException | ClassNotFoundException | IllegalAccessException ex) {
            System.err.println(ex);
            return categories;
        }
    }
    
        public ArrayList<Attribute> getTags(){
        ArrayList<Attribute> attributes = new ArrayList<>();
        try {
            connect();
            PreparedStatement queryTags = db.prepareStatement("SELECT DISTINCT attribute,value FROM "+ this.tableAttributes+" ORDER BY attribute");
            ResultSet rs = queryTags.executeQuery();
            while(rs.next()){
                attributes.add(new Attribute(rs.getString("attribute"),rs.getString("value")));
            }
            return attributes;
        } catch (SQLException | InstantiationException | ClassNotFoundException | IllegalAccessException ex) {
            System.err.println(ex);
            return attributes;
        }
    }
    
    public int getCount(){
        try {
            connect();
            PreparedStatement count = db.prepareStatement("SELECT COUNT(id) FROM "+this.tablename);

            ResultSet rs = count.executeQuery();
            if(rs.next()){
                return rs.getInt(1);
            }else{
                return -1;
            }
        } catch (SQLException | InstantiationException | ClassNotFoundException | IllegalAccessException ex) {
            System.err.println(ex);
            return -1;
        }
    }
    
    public void insertImages(ArrayList<ImageRecord> imageList){
        for(ImageRecord ir:imageList){
            insertImage(ir);
        }
    }
    
    public void deletePhoto(int id){
        try{
            connect();
            PreparedStatement deletePhotoByID = db.prepareStatement("DELETE FROM "+ this.tablename +" WHERE id = ?");

            deleteAttributes(id);

            deletePhotoByID.clearBatch();
            deletePhotoByID.setInt(1, id);
            deletePhotoByID.executeUpdate();
        }catch(SQLException | InstantiationException | ClassNotFoundException | IllegalAccessException e){
            System.err.println();
        }
        System.out.println("Deleted: "+id);
    }
    
    private void deleteAttributes(int id){
        try{
            connect();
            PreparedStatement deleteAttributesByID = db.prepareStatement("DELETE FROM "+ this.tableAttributes +" WHERE id = ?");

            deleteAttributesByID.clearBatch();
            deleteAttributesByID.setInt(1, id);
            deleteAttributesByID.executeUpdate();
        }catch(SQLException | InstantiationException | ClassNotFoundException | IllegalAccessException e){
            System.err.println(e);
        }
    }
     
    public void insertAttributes(int id, AttributeList list){

        try{
            connect();
            PreparedStatement insertAttributes = db.prepareStatement("INSERT INTO " + this.tableAttributes + " (id,attribute,value) Values (?,?,?)");

            for(Attribute attr:list){
                try{
                    insertAttributes.clearBatch();
                    insertAttributes.setInt(1, id);
                    insertAttributes.setString(2, attr.attribute);
                    insertAttributes.setString(3, attr.value);
                    insertAttributes.executeUpdate();
                }catch(SQLException e){
                    System.err.println(e);
                }
            }
        }catch(SQLException | InstantiationException | ClassNotFoundException | IllegalAccessException e){
            System.err.println(e);
        }
    }
    
    public int insertImage(ImageRecord image){
        try{
            connect();
            PreparedStatement insertPhoto = db.prepareStatement("INSERT INTO " + this.tablename + " (type,username,source,sourceID,category,path,views,favorites,date) VALUES (?,?,?,?,?,?,?,?,?)");

            insertPhoto.clearBatch();
            insertPhoto.setInt(1, image.getType());
            insertPhoto.setString(2, image.getUsername());
            insertPhoto.setString(3, image.getSource());
            insertPhoto.setString(4, image.getSourceID());
            insertPhoto.setString(5, image.getCategory());
            insertPhoto.setString(6, image.getPath());
            insertPhoto.setInt(7, image.getViews());
            insertPhoto.setInt(8, image.getFavorites());
            insertPhoto.setDate(9, image.getDate());
            // execute insert SQL stetement
            insertPhoto.executeUpdate();
            ResultSet rs = insertPhoto.getGeneratedKeys();
            if(rs.next()){
                Integer id = rs.getInt(1);
                insertAttributes(id,image.getAttributes());
                System.out.println("Inserted: "+id);
                return id;
            }
        }catch(SQLException | InstantiationException | ClassNotFoundException | IllegalAccessException e){
            System.err.println(e);
        }
        return -1;
    }
    
    private void updateImageMeta(ImageRecord imageRecord) throws SQLException, InstantiationException, ClassNotFoundException, IllegalAccessException{
        try{
            connect();
            PreparedStatement updatePhoto = db.prepareStatement("UPDATE "+this.tablename+" SET type=?,username=?,source=?,sourceID=?,category=?,manual=?,path=?,views=?,favorites=?,date=? WHERE id=?");

            updatePhoto.clearBatch();
            updatePhoto.setInt(1, imageRecord.getType());
            updatePhoto.setString(2, imageRecord.getUsername());
            updatePhoto.setString(3, imageRecord.getSource());
            updatePhoto.setString(4, imageRecord.getSourceID());
            updatePhoto.setString(5, imageRecord.getCategory());
            updatePhoto.setString(6, imageRecord.getManCategory());
            updatePhoto.setString(7, imageRecord.getPath());
            updatePhoto.setInt(8, imageRecord.getViews());
            updatePhoto.setInt(9, imageRecord.getFavorites());
            updatePhoto.setDate(10, imageRecord.getDate());
            updatePhoto.setInt(11,imageRecord.getID());
            updatePhoto.executeUpdate();
        }catch(SQLException | InstantiationException | ClassNotFoundException | IllegalAccessException e){
            System.err.println(e);
            throw e;
        }
    }
    
    public void updateImageAddAttr(ImageRecord imageRecord){
        try{
            updateImageMeta(imageRecord);
            insertAttributes(imageRecord.getID(),imageRecord.getAttributes());
            System.out.println("Updated / Added Attributes: "+imageRecord.getID());
        }catch(SQLException | InstantiationException | ClassNotFoundException | IllegalAccessException e){
            System.err.println(e);
        }
    }
    
    public void updateImageMetaOnly(ImageRecord imageRecord){
        try{
            updateImageMeta(imageRecord);
            System.out.println("Meta Updated: "+imageRecord.getID());
        }catch(SQLException | InstantiationException | ClassNotFoundException | IllegalAccessException e){
            System.err.println(e);
        }
    }
    
    public void updateImageRefreshAttr(ImageRecord imageRecord){
        try{
            updateImageMeta(imageRecord);
            deleteAttributes(imageRecord.getID());
            insertAttributes(imageRecord.getID(),imageRecord.getAttributes());
            System.out.println("Updated / Refreshed Attributes: "+imageRecord.getID());
        }catch(SQLException | InstantiationException | ClassNotFoundException | IllegalAccessException e){
            System.err.println(e);
        }
    }
    
    public AttributeList generateAttrList(int id){
        AttributeList list = new AttributeList();
        try{
            connect();
            PreparedStatement queryAttributesByID = db.prepareStatement("SELECT * FROM "+ this.tableAttributes + " WHERE id IN(?)");

            queryAttributesByID.clearBatch();
            queryAttributesByID.setInt(1, id);
            ResultSet rs = queryAttributesByID.executeQuery();
            while(rs.next()){
                list.add(rs.getString("attribute"), rs.getString("value"));
            }
        }catch(SQLException | InstantiationException | ClassNotFoundException | IllegalAccessException e){
            System.err.println(e);
        }
        return list;
    }
    
    public ImageRecord getImageByID(int id){
        ImageRecord image = null;
        try{
            connect();
            PreparedStatement queryPhotoByID = db.prepareStatement("SELECT * FROM "+ this.tablename + " WHERE id IN(?)");

            queryPhotoByID.clearBatch();
            queryPhotoByID.setInt(1, id);
            ResultSet rs = queryPhotoByID.executeQuery();
            if(rs.next()){
                image = new ImageRecord(rs.getInt("id"),
                        rs.getInt("type"),
                        rs.getString("username"),
                        rs.getString("source"),
                        rs.getString("sourceID"),
                        rs.getString("category"),
                        rs.getString("manual"),
                        rs.getString("path"),
                        rs.getInt("views"),
                        rs.getInt("favorites"),
                        rs.getDate("date"),
                        generateAttrList(id));
               
            }else{
                System.out.println("No record found: "+id);
            }
        }catch(SQLException | InstantiationException | ClassNotFoundException | IllegalAccessException e){
            System.err.println("getImageByID failed.");
            System.err.println(e);
        }
        return image;
    }
    
    public ArrayList<ImageRecord> getPage(int start, int size){
        ArrayList<ImageRecord> collection = new ArrayList<>();
        try{
            connect();
            PreparedStatement queryPage = db.prepareStatement("SELECT * FROM "+ this.tablename +" WHERE id>=? ORDER BY id LIMIT ?");

            queryPage.clearBatch();
            queryPage.setInt(1, start);
            queryPage.setInt(2, size);
            ResultSet rs = queryPage.executeQuery();
            while(rs.next()){
                try{
                    collection.add(new ImageRecord(
                            rs.getInt("id"),
                            rs.getInt("type"),
                            rs.getString("username"),
                            rs.getString("source"),
                            rs.getString("sourceID"),
                            rs.getString("category"),
                            rs.getString("manual"),
                            rs.getString("path"),
                            rs.getInt("views"),
                            rs.getInt("favorites"),
                            rs.getDate("date"),
                            generateAttrList(rs.getInt("id"))));
                }catch(SQLException e){
                    System.err.println("ResultSet record cannot be converted to ImageRecord");
                    System.err.println(e);
                }
            }
        }catch(SQLException | InstantiationException | ClassNotFoundException | IllegalAccessException e){
            System.err.println("getPage query to database failed.");
            System.err.println(e);
        }
        return collection;
    }
    
        public ArrayList<ImageRecord> getPageTag(int start, int size, String attribute, String value){
        ArrayList<ImageRecord> collection = new ArrayList<>();
        try{
            connect();
            PreparedStatement queryPageTag = db.prepareStatement("SELECT "+this.tablename+".* FROM "+ this.tablename+" LEFT JOIN "+this.tableAttributes+" ON "+this.tablename+".id="+this.tableAttributes+".id WHERE "+this.tablename+".id>=? AND attribute=? AND value=? GROUP BY "+this.tablename+".id ORDER BY "+this.tablename+".id LIMIT ?");

            queryPageTag.clearBatch();
            queryPageTag.setInt(1, start);
            queryPageTag.setString(2, attribute);
            queryPageTag.setString(3, value);
            queryPageTag.setInt(4, size);
            ResultSet rs = queryPageTag.executeQuery();
            while(rs.next()){
                try{
                    collection.add(new ImageRecord(
                            rs.getInt("id"),
                            rs.getInt("type"),
                            rs.getString("username"),
                            rs.getString("source"),
                            rs.getString("sourceID"),
                            rs.getString("category"),
                            rs.getString("manual"),
                            rs.getString("path"),
                            rs.getInt("views"),
                            rs.getInt("favorites"),
                            rs.getDate("date"),
                            generateAttrList(rs.getInt("id"))));
                }catch(SQLException e){
                    System.err.println("ResultSet record cannot be converted to ImageRecord");
                    System.err.println(e);
                }
            }
        }catch(SQLException | InstantiationException | ClassNotFoundException | IllegalAccessException e){
            System.err.println("getPageTag query to database failed.");
            System.err.println(e);
        }
        return collection;
    }
    
    public ArrayList<ImageRecord> getPageCategory(int start, int size, String category){
        ArrayList<ImageRecord> collection = new ArrayList<>();
        try{
            connect();
            PreparedStatement queryPageCategory = db.prepareStatement("SELECT * FROM "+ this.tablename +" WHERE id>=? AND category=? ORDER BY id LIMIT ?");

            queryPageCategory.clearBatch();
            queryPageCategory.setInt(1, start);
            queryPageCategory.setString(2, category);
            queryPageCategory.setInt(3, size);
            ResultSet rs = queryPageCategory.executeQuery();
            while(rs.next()){
                try{
                    collection.add(new ImageRecord(
                            rs.getInt("id"),
                            rs.getInt("type"),
                            rs.getString("username"),
                            rs.getString("source"),
                            rs.getString("sourceID"),
                            rs.getString("category"),
                            rs.getString("manual"),
                            rs.getString("path"),
                            rs.getInt("views"),
                            rs.getInt("favorites"),
                            rs.getDate("date"),
                            generateAttrList(rs.getInt("id"))));
                }catch(SQLException e){
                    System.err.println("ResultSet record cannot be converted to ImageRecord");
                    System.err.println(e);
                }
            }
        }catch(SQLException | InstantiationException | ClassNotFoundException | IllegalAccessException e){
            System.err.println("getPageCategory query to database failed.");
            System.err.println(e);
        }
        return collection;
    }

    public ArrayList<ImageRecord> getPageCatgTag(int start, int size, String category, String attribute, String value){
        ArrayList<ImageRecord> collection = new ArrayList<>();
        try{
            connect();
            PreparedStatement queryPageCategoryTag = db.prepareStatement("SELECT "+this.tablename+".* FROM "+ this.tablename+" LEFT JOIN "+this.tableAttributes+" ON "+this.tablename+".id="+this.tableAttributes+".id WHERE "+this.tablename+".id>=? AND category=? AND attribute=? AND value=? GROUP BY "+this.tablename+".id ORDER BY "+this.tablename+".id LIMIT ?");

            queryPageCategoryTag.clearBatch();
            queryPageCategoryTag.setInt(1, start);
            queryPageCategoryTag.setString(2, category);
            queryPageCategoryTag.setString(3, attribute);
            queryPageCategoryTag.setString(4, value);
            queryPageCategoryTag.setInt(5, size);
            ResultSet rs = queryPageCategoryTag.executeQuery();
            while(rs.next()){
                try{
                    collection.add(new ImageRecord(
                            rs.getInt("id"),
                            rs.getInt("type"),
                            rs.getString("username"),
                            rs.getString("source"),
                            rs.getString("sourceID"),
                            rs.getString("category"),
                            rs.getString("manual"),
                            rs.getString("path"),
                            rs.getInt("views"),
                            rs.getInt("favorites"),
                            rs.getDate("date"),
                            generateAttrList(rs.getInt("id"))));
                }catch(SQLException e){
                    System.err.println("ResultSet record cannot be converted to ImageRecord");
                    System.err.println(e);
                }
            }
        }catch(SQLException | InstantiationException | ClassNotFoundException | IllegalAccessException e){
            System.err.println("getPageTag query to database failed.");
            System.err.println(e);
        }
        return collection;
    }
    
    public ArrayList<ImageRecord> getAllMeta(){
        ArrayList<ImageRecord> collection = new ArrayList<>();
        try{
            connect();
            PreparedStatement queryAll = db.prepareStatement("SELECT * FROM "+ this.tablename);

            queryAll.clearBatch();

            ResultSet rs = queryAll.executeQuery();
            while(rs.next()){
                try{
                    collection.add(new ImageRecord(
                            rs.getInt("id"),
                            rs.getInt("type"),
                            rs.getString("username"),
                            rs.getString("source"),
                            rs.getString("sourceID"),
                            rs.getString("category"),
                            rs.getString("manual"),
                            rs.getString("path"),
                            rs.getInt("views"),
                            rs.getInt("favorites"),
                            rs.getDate("date"),
                            generateAttrList(rs.getInt("id"))));
                }catch(SQLException e){
                    System.err.println("ResultSet record cannot be converted to ImageRecord");
                    System.err.println(e);
                }
            }
        }catch(SQLException | InstantiationException | ClassNotFoundException | IllegalAccessException e){
            System.err.println("getAll query to database failed.");
            System.err.println(e);
        }
        return collection;
    }
    
    public String getTable(){
        return this.tablename;
    }
    
    public void close(){
        db.close();
    }
}


