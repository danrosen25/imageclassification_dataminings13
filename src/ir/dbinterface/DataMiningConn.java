/*
* ========== Database Connection ==========
* Image Classfication
* Daniel Rosen, Shawn Kim, Samriti Kanwar
* CSCI 4502/5502: Data Mining Homework 
* Spring 2013
* Author: Dan Rosen
*/

package ir.dbinterface;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class DataMiningConn{
    private Connection dbConn = null;
    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private String sqluser;
    private String server;
    private String port;
    private String database;
    private String password;
    //private Statement dbStatement;
    
    public DataMiningConn()
    {
        this("drosen","DataMining3","205.178.146.102","3306","data_mining");
    }
        
    public DataMiningConn(String sqluser, String password, String server, String port, String database)
    {
        this.sqluser = sqluser;
        this.password = password;
        this.server = server;
        this.port = port;
        this.database = database;
    }
    
    public void connect() throws SQLException,InstantiationException,ClassNotFoundException,IllegalAccessException{
        if(dbConn==null || dbConn.isClosed()) {   
            String DB_URL = "jdbc:mysql://"+server+":"+port+"/"+database;
            Class.forName(JDBC_DRIVER).newInstance();
            // Setup the connection with the DB
            dbConn = DriverManager.getConnection(DB_URL,sqluser,password);
            System.out.println("Connect to: "+database);
        }else if(dbConn.isClosed()){
            String DB_URL = "jdbc:mysql://"+server+":"+port+"/"+database;
            Class.forName(JDBC_DRIVER).newInstance();
            // Setup the connection with the DB
            dbConn = DriverManager.getConnection(DB_URL,sqluser,password);
            System.out.println("Reconnect to: "+database);
        }
    }
    
    public PreparedStatement prepareStatement(String sql) throws SQLException{
            return dbConn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
    }
    
    public void close(){
        
        try{
            if(dbConn!=null){dbConn.close();}
        }catch(SQLException e){
            System.err.println(e);
        }
    }
}
