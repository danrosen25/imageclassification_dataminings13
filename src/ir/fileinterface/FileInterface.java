/*
 * ========== Interface to load/safe photos to/from file ==========
* Image Classfication
* Daniel Rosen, Shawn Kim, Samriti Kanwar
* CSCI 4502/5502: Data Mining Homework 
* Spring 2013
* Author: Dan Rosen
*/

package ir.fileinterface;

import ir.structures.ImageRecord;
import ir.dbinterface.PhotoTableInterface;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class FileInterface { 
    FileInterface(){

    }
    
    //This will no longer work properly due to changes in ImageRecords/Database
    public static void loadFile(String file){
        PhotoTableInterface pt = new PhotoTableInterface("photoSet","photoSetAttr");
        String stringRead = null;
        long lineNumber = 0;
        try{
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            do{
                lineNumber++;
                try{
                    stringRead = br.readLine();
                    try{
                        if(stringRead != null){
                            ImageRecord ir = new ImageRecord(stringRead);
                            pt.insertImage(ir);
                        }
                    }catch(Exception e){
                        System.err.println("Could not load line: "+lineNumber);
                    }                 
                }catch(IOException e){
                    System.err.println("Cannot Read Line: "+lineNumber);   
                }       
            }while(stringRead != null);
            
            try{
                br.close();
                fr.close();
            }catch(IOException e){
                System.err.println(e);
            }
        }catch(FileNotFoundException e){
            System.err.println(e);
        }
    }
    
    public static void outFile(String file,PhotoTableInterface photoset){
        ArrayList<ImageRecord> photos = photoset.getAllMeta();
        try{
            PrintWriter pw = new PrintWriter(file);
            for(ImageRecord ir:photos){
                pw.println(ir.toString());
            }
            pw.close();
        }catch(IOException e){
            System.err.println(e);
        }
    }
}
